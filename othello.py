
NONE = '.'
BLACK = 'B'
WHITE = 'W'
MOST_CELLS = 'M'
LEAST_CELLS = 'L'

# An Exception that is raised every time an invalid move occurs

class InvalidMoveException(Exception):
    ''' Raised whenever an exception arises from an invalid move '''
    pass
#  class that manages the game

class OthelloGame:
    '''
    Class that creates the Othello game and deals with all its game logic
    '''
    def __init__(self, rows: int, cols: int, turn: str,
                 top_left: str, victory_type: str):
        ''' Initialize all of the games settings and creates the board. '''
        self.rows = rows
        self.cols = cols
        self.current_board = self._new_game_board(rows, cols, top_left)
        self.turn = turn
        self.victory_type = victory_type

    def _new_game_board(self, rows: int, cols: int, top_left: str) -> str :
        ''' Creates the Othello Game board with specified dimensions. '''
        board =[]
        # Create an empty board
        for row in range(rows):
            board.append([])
            for col in range(cols):
                board[-1].append(NONE)

        # Initialize the 4 game pieces in the center
        board[rows // 2 - 1][cols // 2 - 1] = top_left
        board[rows // 2 - 1][cols // 2] = self._opposite_turn(top_left)
        board[rows // 2][cols // 2 - 1] = self._opposite_turn(top_left)
        board[rows // 2][cols // 2] = top_left
        
        return board

    def move(self, row: int, col: int) -> None:
        self._require_valid_empty_space_to_move(row, col)
        possible_directions = self._adjacent_opposite_color_directions(row, col, self.turn)
        next_turn = self.turn
        for direction in possible_directions:
            if self._is_valid_directional_move(row, col, direction[0], direction[1], self.turn):
                next_turn = self._opposite_turn(self.turn)
            self._convert_adjacent_cells_in_direction(row, col, direction[0], direction[1], self.turn)
        if next_turn != self.turn:
            self.current_board[row][col] = self.turn
            if self.can_move(next_turn):
                self.switch_turn()
        else:
            raise InvalidMoveException()

    def _is_valid_directional_move(self, row: int, col: int, rowdelta: int, coldelta: int, turn: str) -> bool:
        current_row = row + rowdelta
        current_col = col + coldelta

        last_cell_color = self._opposite_turn(turn)
        while True:
            if not self._is_valid_cell(current_row, current_col):
                break
            if self._cell_color(current_row, current_col) == NONE:
                break           
            if self._cell_color(current_row, current_col) == turn:
                last_cell_color = turn
                break
            current_row += rowdelta
            current_col += coldelta   
        return last_cell_color == turn

    def _adjacent_opposite_color_directions(self, row: int, col: int, turn: str) -> tuple:
        dir_list = []
        for rowdelta in range(-1, 2):
            for coldelta in range(-1, 2):
                if self._is_valid_cell(row+rowdelta, col + coldelta):
                    if self.current_board[row + rowdelta][col + coldelta] == self._opposite_turn(turn):
                        dir_list.append((rowdelta, coldelta))
        return dir_list

    def _convert_adjacent_cells_in_direction(self, row: int, col: int,
                                             rowdelta: int, coldelta: int, turn: str) -> None:
        if self._is_valid_directional_move(row, col, rowdelta, coldelta, turn):
            current_row = row + rowdelta
            current_col = col + coldelta
            while self._cell_color(current_row, current_col) == self._opposite_turn(turn):
                self._flip_cell(current_row, current_col)
                current_row += rowdelta
                current_col += coldelta

    def is_game_over(self) -> bool:
        return self.can_move(BLACK) == False and self.can_move(WHITE) == False

    def can_move(self, turn: str) -> bool:
        for row in range(self.rows):
            for col in range(self.cols):
                if self.current_board[row][col] == NONE:
                    for direction in self._adjacent_opposite_color_directions(row, col, turn):
                        if self._is_valid_directional_move(row, col, direction[0], direction[1], turn):
                            return True
        return False

    def return_winner(self) -> str:
        ''' Returns the winner. ONLY to be called once the game is over.
            Returns None if the game is a TIE game.'''
        black_cells = self.get_total_cells(BLACK)
        white_cells = self.get_total_cells(WHITE)

        if black_cells == white_cells:
            return None
        elif self.victory_type == MOST_CELLS:
            if black_cells > white_cells:
                return BLACK
            else:
                return WHITE
        else:
            if black_cells < white_cells:
                return BLACK
            else:
                return WHITE

    def switch_turn(self) -> None:
        self.turn = self._opposite_turn(self.turn)

    def get_board(self) -> str:
        ''' Returns the current game's 2D board '''
        return self.current_board

    def get_rows(self) -> int:
        ''' Returns the number of rows the game currently has '''
        return self.rows

    def get_columns(self) -> int:
        ''' Returns the number of columns the game currently has '''
        return self.cols

    def get_turn(self) -> str:
        ''' Returns the current game's turn '''
        return self.turn

    def get_total_cells(self, turn: str) -> int:
        ''' Returns the total cell count of the specified colored player '''
        total = 0
        for row in range(self.rows):
            for col in range(self.cols):
                if self.current_board[row][col] == turn:
                    total += 1
        return total

    def _flip_cell(self, row: int, col: int) -> None:
        ''' Flips the specified cell over to the other color '''
        self.current_board[row][col] = self._opposite_turn(self.current_board[row][col])
        
    def _cell_color(self, row: int, col: int) -> str:
        ''' Determines the color/player of the specified cell '''
        return self.current_board[row][col]

    def _opposite_turn(self, turn: str) -> str:
        ''' Returns the player of the opposite player '''
        return {BLACK: WHITE, WHITE: BLACK}[turn]

    def _require_valid_empty_space_to_move(self, row: int, col: int) -> bool:
        ''' In order to move, the specified cell space must be within board boundaries
            AND the cell has to be empty '''
        if self._is_valid_cell(row, col) and self._cell_color(row, col) != NONE:
            raise InvalidMoveException()

    def _is_valid_cell(self, row: int, col: int) -> bool:
        ''' Returns True if the given cell move position is invalid due to
            position (out of bounds) '''
        return self._is_valid_row_number(row) and self._is_valid_col_number(col)

    def _is_valid_row_number(self, row: int) -> bool:
        ''' Returns True if the given row number is valid; False otherwise '''
        return 0 <= row < self.rows
        
    def _is_valid_col_number(self, col: int) -> bool:
        ''' Returns True if the given col number is valid; False otherwise '''
        return 0 <= col < self.cols

    
